	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/', function () {
    return view('welcome');
});


*/


Route::get('/', 'ZoneController@index')->name('login');

Route::post('/login', 'ZoneController@login')->name('to_login');

Route::group(['middleware' => ['usersession']], function () {

Route::get('/AllZone', 'ZoneController@AllZone')->name('AllZone');
Route::get('/Zone1Only', 'ZoneController@Zone1Only')->name('Zone1Only');
Route::get('/Zone2Only', 'ZoneController@Zone2Only')->name('Zone2Only');
Route::get('/Zone3Only', 'ZoneController@Zone3Only')->name('Zone3Only');
Route::get('/Zone4Only', 'ZoneController@Zone4Only')->name('Zone4Only');
Route::get('/Zone5Only', 'ZoneController@Zone5Only')->name('Zone5Only');
Route::get('/Zone6Only', 'ZoneController@Zone6Only')->name('Zone6Only');
Route::get('/Zone7Only', 'ZoneController@Zone7Only')->name('Zone7Only');
Route::get('/Zone1', 'ZoneController@Zone1')->name('Zone1');
Route::get('/Zone2', 'ZoneController@Zone2')->name('Zone2');
Route::get('/Zone3', 'ZoneController@Zone3')->name('Zone3');
Route::get('/Zone4', 'ZoneController@Zone4')->name('Zone4');
Route::get('/Zone5', 'ZoneController@Zone5')->name('Zone5');
Route::get('/Zone6', 'ZoneController@Zone6')->name('Zone6');
Route::get('/Zone7', 'ZoneController@Zone7')->name('Zone7');

Route::get('get_by_id_Guest', ['as' => 'get_by_id_Guest', 'uses' => 'ZoneController@get_by_id_Guest']);

Route::get('get_by_id', ['as' => 'get_by_id', 'uses' => 'ZoneController@get_by_id']);

Route::post('save_data', 'ZoneController@save_data')->name('save_data_post');

Route::get('save_data', 'ZoneController@save_data')->name('save_data');

});

Route::post('/logout', 'ZoneController@logout')->name('logout');

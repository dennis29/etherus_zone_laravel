<hr>
<hr>
<div style="float:left;margin-left:10px;">
<section id="tabs" style='margin-top:50px'>
	<div class="container" id="contains">
		<div class="row">
			<div class="col-xs-12 ">
				<nav>
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
						<a class="nav-item nav-link active" id="nav-add-tab" data-toggle="tab" href="#nav-add" role="tab" aria-controls="nav-add" aria-selected="true">Add</a>
						<a class="nav-item nav-link" id="nav-del-tab" data-toggle="tab" href="#nav-del" role="tab" aria-controls="nav-del" aria-selected="false">Delete</a>
						<a class="nav-item nav-link" id="nav-merge-tab" data-toggle="tab" href="#nav-merge" role="tab" aria-controls="nav-merge" aria-selected="false">Merge</a>
						<a class="nav-item nav-link" id="nav-split-tab" data-toggle="tab" href="#nav-split" role="tab" aria-controls="nav-split" aria-selected="false">Split</a>
					</div>
				</nav>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-add" role="tabpanel" aria-labelledby="nav-add-tab">
					<form class="form-horizontal" id='form_add'>
					      <div class="form-group col-sm-10">
							  <label for="table_type">Select Table's Type :</label>
							  <select class="form-control" id="table_type">
								<option value="Circle_four">Circle</option>
								<option value="Square_four">Square</option>
							  </select>
						  </div>
						  <div class="form-group">
							<label class="control-label col-sm-7" for="table_no">Table No</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" id="table_no" placeholder="Enter Table No">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="add_div" onclick="add_new_div()">Add</button>
							</div>
						</div>

					</div>
					<div class="tab-pane fade" id="nav-del" role="tabpanel" aria-labelledby="nav-del-tab">
						<form class="form-horizontal" id='form_del'>
						  <div class="form-group">
							<label class="control-label col-sm-7" for="table_no">Table No</label>
							<div class="col-sm-10">
							  <input type="text" class="form-control" id="table_no" placeholder="Enter Table No">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="del_div" onclick="delete_div()">Delete</button>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="nav-merge" role="tabpanel" aria-labelledby="nav-merge-tab">
					<form class="form-horizontal" id='form_merge'>
						  <div class="form-group">
							<label class="control-label col-sm-12" for="tables_no">Enter Tables (Separate With Commas)</label>
							<div class="col-sm-12">
							  <input type="text" class="form-control" id="tables_no" value="" placeholder="Enter Tables">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="mer_div" onclick="merge_div()">Merge</button>
							</div>
						</div>
				    </div>
					<div class="tab-pane fade" id="nav-split" role="tabpanel" aria-labelledby="nav-split-tab">
					<form class="form-horizontal" id='form_split'>
						  <div class="form-group">
							<label class="control-label col-sm-12" for="tables_no">Enter Tables In Merge (Separate With Commas)</label>
							<div class="col-sm-12">
							  <input type="text" class="form-control" id="tables_split" value="" placeholder="Enter Tables">
							</div>
						  </div>
						</form>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-primary" id="spl_div" onclick="split_div()">Split</button>
							</div>
						</div>
				    </div>
				</div>

			</div>
		</div>
	</div>
</section>
</div>

<script>

function change_color(id="")
{
	if(document.getElementById(id).className  == "border_red")
	{
		document.getElementById(id).className  = "border_green";
	}
	else if(document.getElementById(id).className  == "border_green")
	{
		document.getElementById(id).className  = "border_blue";
	}
	else
	{
		document.getElementById(id).className  = "border_red";
	}
	
	


}

var check_add = 0;
var style_x = "";
function add_new_div(id="",table_type=""){
	var tbl_type = table_type;
	if(!id)
	{
	   var id = $('#form_add #table_no').val();
	}
	if(!tbl_type)
	{
	  tbl_type = $('#table_type').val();
	}
if(check_add > 0){
	style_x = 'style="margin-top:-60px"';
}
	$('#new_table').append('<div id="t'+id+'" '+style_x+' onclick="" class="green"><img src="assets/images/'+tbl_type+'_green.jpg" width="78" height="68" class='+JSON.stringify(tbl_type)+' />'+id+'</div>');
	$("#t"+id).draggable();
	$('#t'+id).attr('onclick','change_new_table_col('+id+','+JSON.stringify(tbl_type)+')');
	
	
check_add++; 	

}


function change_new_table_col(id=0,table_type=""){
  if($("#t"+id).attr('class')=="green")
			{

			   $("#t"+id).attr('class','red');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_red.jpg" width="78" height="68" class="'+table_type+'" alt="">'+id);


			}
			else if($("#t"+id).attr('class')=="red")
			{

			   $("#t"+id).attr('class','blue');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_blue.jpg" width="78" height="68" class="'+table_type+'"  alt="">'+id);

			}
			else{

			   $("#t"+id).attr('class','green');
			   $("#t"+id).html('<img src="assets/images/'+table_type+'_green.jpg" width="78" height="68" class="'+table_type+'"  alt="">'+id);
			}
			
			


}

function delete_div(){

	var id = $('#form_del #table_no').val();
	var img = $('#t'+id).find("img");
	$('#t'+id).html('<div style="width:78px;height:68px">&nbsp</div>');
	$('#t'+id).attr('id','');
}

function split_div(){
	var arr = $('#tables_split').val().split(',');
	
	i=0;
	
	var id_div = arr.join('_');	
	
	console.log(123213213);
	
	$('#t'+id_div+'_merge img').each(function() {
	    add_new_div(arr[i],$(this).attr('class'));
		//return false;
		i++;
    });

	$('#t'+id_div+'_merge').html('<div style="width:156px;height:68px">&nbsp</div>');
	$('#t'+id_div+'_merge').attr('id','');
	
	
}


//localStorage[zone] = "";

function merge_div(){

	var arr = $('#tables_no').val().split(',');
	
	var arr_table_type = [];

	$('#tables_no').val('');

	var width = 0;

	var div="";

	for(i=0;i<arr.length;i++)
	{
		    console.log($('#t'+arr[i]+' img').attr('class'));
	        arr_table_type[i] = $('#t'+arr[i]).children('img').attr('class');  
	        div +='<div style="float:left;"><img class="'+	[i]+'" src="assets/images/'+$('#t'+arr[i]+' img').attr('class')+'_green.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';			
			$("#t"+arr[i]).html('<div style="width:78px;height:68px">&nbsp</div>');
			$("#t"+arr[i]).attr('id','');
	}
	var id_div = arr.join('_');

	$('#table_merge').append('<div class="green" style="margin-bottom:-65px" id="t'+id_div+'_merge" onclick="">'+div+'</div>');	
	
	$('#t'+id_div+'_merge').attr('onclick',"change_color_dm(["+arr+"],"+JSON.stringify(arr_table_type)+")");

	$('#t'+id_div+'_merge').draggable();
	
	

}

function change_color_dm(arr=[],arr_table_type=[]){

   var id_div = arr.join('_');
    if($('#t'+id_div+'_merge').attr('class')=="green")
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','blue');
		      for(i=0;i<arr.length;i++)
			  {
				  console.log(arr_table_type[i]);
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_blue.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }
		   else if($('#t'+id_div+'_merge').attr('class')=="blue")
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','red');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_red.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }
		   else
		   {
		      var div = "";
		      $('#t'+id_div+'_merge').attr('class','green');
		      for(i=0;i<arr.length;i++)
			  {
				div +='<div style="float:left;"><img class='+JSON.stringify(arr_table_type[i])+' src="assets/images/'+arr_table_type[i]+'_green.jpg" width="78" height="68" alt="">'+arr[i]+'</div>';
			  }
			  $('#t'+id_div+'_merge').html(div);
		   }	   
		   
		   

}

function div_click(id){
	if($("#t"+id).attr('class')=="border_blue rectangle")
	{

	   $("#t"+id).attr('class','border_red rectangle');

	}
	else if($("#t"+id).attr('class')=="border_green rectangle")
	{

	   $("#t"+id).attr('class','border_blue rectangle');

	}
	else{
	   $("#t"+id).attr('class','border_green rectangle');
	}
	
		

}
</script>
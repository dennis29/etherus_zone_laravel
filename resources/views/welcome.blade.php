<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        
<div style="margin-left:40px">
<form action="/dm" method="GET">
  <div class="form-group">
    <label for="exampleInputEmail1">Address</label>
    <div class="input_fields_wrap">
    <button class="add_field_button">Add More Fields</button>
		<div>
			<textarea name="desc[]" id="desc" class="form-control" style="width:200px" required></textarea>
		</div>
	</div>  
	</div>
  <div class="form-group">
    <label for="date-address">Birth Date</label>
    <input type="date" name="date-address" class="form-control" id="date-address" style="width:200px" required/>
  </div>
  <div class="form-group">
    <label for="membership">membership</label>
    <select class="form-control" id="membership" name="membership" style="width:200px" autocomplete="on" required>
	<option value="">== Select ==</option>
	<option value="silver">Silver</option>
	<option value="gold">Gold</option>
	<option value="platinum">Platinum</option>
	<option value="black">Black</option>
	<option value="vip">VIP</option>
	<option value="vvip">VVIP</option>
	</select>
  </div>
  <div class="form-group">
    <label for="date-address">Credit Card</label>
    <input type="input" name="credit_card" class="form-control" pattern="[0-9]*" id="credit_card" style="width:200px" required/>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div>' +
                '<textarea name="desc[]" id="desc" class="form-control" style="width:200px" required></textarea>' +
                '<a href="#" class="remove_field">Remove</a>' +
            '</div>');
    }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });
	
	
	$("#credit_card").keypress(function (e) {
		if(e.which == 46){
			if($(this).val().indexOf('.') != -1) {
				return false;
			}
		}

		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
});
</script>
    </body>
</html>

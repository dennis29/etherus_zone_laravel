INSERT INTO migrations VALUES (2, '2019_11_26_060329_create_zones_table', 1);
INSERT INTO migrations VALUES (3, '2019_11_26_060506_create_zone_permissions_table', 1);
INSERT INTO migrations VALUES (4, '2019_11_26_060556_create_zone_permission_roles_table', 1);
INSERT INTO migrations VALUES (5, '2019_11_26_060716_create_roles_table', 1);
INSERT INTO migrations VALUES (6, '2019_11_26_060826_create_user_roles_table', 1);

INSERT INTO roles VALUES (1, 'console', NULL, NULL);
INSERT INTO roles VALUES (2, 'zone1', NULL, NULL);
INSERT INTO roles VALUES (3, 'zone2', NULL, NULL);
INSERT INTO roles VALUES (4, 'zone3', NULL, NULL);
INSERT INTO roles VALUES (5, 'zone4', NULL, NULL);
INSERT INTO roles VALUES (6, 'zone6', NULL, NULL);
INSERT INTO roles VALUES (7, 'zone6', NULL, NULL);
INSERT INTO roles VALUES (8, 'zone7', NULL, NULL);
INSERT INTO roles VALUES (9, 'admin', NULL, NULL);


INSERT INTO user_roles VALUES (1, 1, 1, NULL, NULL);
INSERT INTO user_roles VALUES (2, 2, 2, NULL, NULL);
INSERT INTO user_roles VALUES (3, 3, 3, NULL, NULL);
INSERT INTO user_roles VALUES (4, 4, 4, NULL, NULL);
INSERT INTO user_roles VALUES (5, 5, 5, NULL, NULL);
INSERT INTO user_roles VALUES (6, 6, 6, NULL, NULL);
INSERT INTO user_roles VALUES (7, 7, 7, NULL, NULL);
INSERT INTO user_roles VALUES (8, 8, 8, NULL, NULL);
INSERT INTO user_roles VALUES (9, 9, 9, NULL, NULL);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'console', 'myconsole', NULL, NULL);
INSERT INTO users VALUES (2, 'zone1', '11111', NULL, NULL);
INSERT INTO users VALUES (3, 'zone2', '22222', NULL, NULL);
INSERT INTO users VALUES (4, 'zone3', '33333', NULL, NULL);
INSERT INTO users VALUES (5, 'zone4', '44444', NULL, NULL);
INSERT INTO users VALUES (6, 'zone5', '55555', NULL, NULL);
INSERT INTO users VALUES (7, 'zone6', '66666', NULL, NULL);
INSERT INTO users VALUES (8, 'zone7', '77777', NULL, NULL);
INSERT INTO users VALUES (9, 'etherus', '12345', NULL, NULL);


--
-- Data for Name: zone_permission_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO zone_permission_roles VALUES (1, 1, 1, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (2, 2, 2, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (3, 3, 3, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (4, 4, 4, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (5, 5, 5, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (6, 6, 6, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (7, 7, 7, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (8, 8, 8, NULL, NULL);
INSERT INTO zone_permission_roles VALUES (9, 9, 9, NULL, NULL);


--
-- Data for Name: zone_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO zone_permissions VALUES (1, 'AllZone', NULL, NULL);
INSERT INTO zone_permissions VALUES (2, 'Zone1Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (3, 'Zone2Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (4, 'Zone3Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (5, 'Zone4Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (6, 'Zone5Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (7, 'Zone6Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (8, 'Zone7Only', NULL, NULL);
INSERT INTO zone_permissions VALUES (9, 'Zone1', NULL, NULL);
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

use File;

use Storage;

class user extends Model
{
    protected $fillable = [
        'username', 'password'
    ];
	
	public function login($array){
		
	//DB::enableQueryLog();	
	 return DB::table('users')
		->select('zone_permissions.name as dir_url')
		->join('user_roles', 'users.id', '=', 'user_roles.user_id')
		->join('zone_permission_roles', 'zone_permission_roles.role_id', '=', 'user_roles.role_id')
		->join('zone_permissions', 'zone_permissions.id', '=', 'zone_permission_roles.zone_permission_id')
		->where('users.username',$array['username'])->where('users.password', $array['password'])
		->first();
		//dd(DB::getQueryLog());
	}
	
	public function get_by_id($array){
		if(isset($array['id'])){
		//	$sql_script = "select zone".$array['zone_id']." as zone1 from zone WHERE zone_id = ".$array['id'];
		    $file_loc ='assets/zone/zone1.html';
			$file=fopen($file_loc,"rb");
			$data['0']['body'] = fread($file,filesize($file_loc));
			fclose($file);
		}
		else
		{
			 $file_loc ='assets/zone/zone1.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
			   $data['0']['body'] = fread($file,filesize($file_loc));
		     else
			   $data['0']['body'] = "";
			 fclose($file);
			 
			 $file_loc ='assets/zone/zone2.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
				$data['1']['body'] = fread($file,filesize($file_loc));
			 else
				$data['1']['body'] = "";
			 fclose($file);
			 
			 $file_loc ='assets/zone/zone3.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
				$data['2']['body'] = fread($file,filesize($file_loc));
			 else
				$data['2']['body'] = ""; 
			 fclose($file);
			 
			 $file_loc ='assets/zone/zone4.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
				$data['3']['body'] = fread($file,filesize($file_loc));
			 else
				$data['3']['body'] = ""; 
			 fclose($file);
			 
			 $file_loc ='assets/zone/zone5.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
		        $data['4']['body'] = fread($file,filesize($file_loc)); 
			 else
				$data['4']['body'] = "";  
			 fclose($file);
			 
			 $file_loc ='assets/zone/zone6.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
				$data['5']['body'] = fread($file,filesize($file_loc));
			 else
				$data['5']['body'] = "";  
			 fclose($file);
			 
			 $file_loc ='assets/zone/zone7.html';
			 $file=fopen($file_loc,"rb");
			 if(filesize($file_loc) > 0)
				$data['6']['body'] = fread($file,filesize($file_loc));
			 else
				$data['6']['body'] = "";
			
			 fclose($file);
		} 
		
		 return $data;
	}
	

	public function get_by_id_Guest($array){
		    
			
		    $file_loc = 'assets/zone/zone'.$array['id'].'.html';
			$data=File::get($file_loc);
			//dd($file);
			
			//$sql_script = "UPDATE zone set ".$array['author']."_sync = 0 WHERE zone_id = ".$array['id'];
			$sql_script = DB::select( DB::raw("UPDATE zones set ".$array['author']."_sync = 0 WHERE id = ".$array['id']));
		//	$this->db->query( $sql_script );
		
	    	return $data;
	}
	
	public function save_data($array){
		
		$sql_script = "select * from zones WHERE id = ".$array['id']." and admin_sync = 0 and guest_sync = 0";
		$get = DB::select( DB::raw($sql_script));
		
		if($get){
			$sql_script = "UPDATE zones set admin_sync = 1,guest_sync = 1 where id=".$array['id'];
			
		}
		else
		{
		  $sql_script = "select * from zones WHERE id = ".$array['id'];	
		  $get_data = DB::select( DB::raw($sql_script));
		  //echo json_encode($array['body']);die();
		  if(count($get_data)==0){
			  $sql_script = 'insert into zones(id) values ('.$array['id'].')';		   
		  }
		}
		//$file=fopen('assets/zone/zone'.$array['id'].'.html',"a");
		$file = @fopen('assets/zone/zone'.$array['id'].'.html', "r+");
		ftruncate($file, 0);
	    fwrite($file,$array['body']);
		//Storage::put('file.txt', 'Your name');
		return DB::select( DB::raw($sql_script));
		
	}
}

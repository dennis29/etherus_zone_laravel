<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class zone extends Model
{
    protected $fillable = [
        'admin_sync', 'guest_sync' ,'hostess_sync'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class zone_permission extends Model
{
    protected $fillable = [
        'name'
    ];
}

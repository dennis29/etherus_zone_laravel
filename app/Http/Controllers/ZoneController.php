<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\user;

use Illuminate\Support\Facades\Input;

use Session;

use Auth;

class ZoneController extends Controller
{
    public function index()
	{
		return view('Login/index');
	}
	
	public function login(Request $request)
	{
		$user = new User();
		$get_data = $user->login($request->post());
		//dd($get_data->dir_url);
	
		if($get_data)
		{
		//	dd($get_data->dir_url);
			if($get_data->dir_url)
			{	
		        $request->session()->put('zone_permission', $get_data->dir_url);
				$request->session()->put('username', $request->post('username'));
				//dd($get_data->dir_url);
				return redirect($get_data->dir_url);
			}
			else
			{
			    return  redirect($get_data['dir_url']);
			}
		}
		else
		{
			return  redirect('/');
		}
	
	}
	
	public function get_by_id_Guest()
	{
		$user = new User();
		
		return response()->json($user->get_by_id_Guest(Input::get()));
	
	}
	
	public function get_by_id()
	{
		$user = new User();
		
		return response()->json($user->get_by_id(Input::get()));
	
	}
	
	public function save_data(Request $request)
	{
		$user = new User();
		echo json_encode($user->save_data($request->post()));
	}
	
	public function Zone1Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1Only"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone1Only/index');
	}
	
	public function Zone2Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone2Only"){
			return  redirect('/');
		}
		return view('Zone2Only/index');
	}
	
	public function Zone3Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone3Only"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone3Only/index');
	}
	
	public function Zone4Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone4Only"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone4Only/index');
	}
	
	public function Zone5Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone5Only"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone5Only/index');
	}
	
	public function Zone6Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone6Only"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone6Only/index');
	}
	
	public function Zone7Only(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone7Only"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone7Only/index');
	}
	
	public function AllZone(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "AllZone"){
			return  redirect('/');
		}
		Session::save();
		return view('AllZone/index');
	}
	
	public function Zone1(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone1/index');
	}
	
	public function Zone2(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone2/index');
	}
	
	public function Zone3(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone3/index');
	}
	
	public function Zone4(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone4/index');
	}
	
	public function Zone5(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone5/index');
	}
	
	public function Zone6(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone6/index');
	}
	
	public function Zone7(Request $request)
	{
		$check_zone_permission =  $request->session()->get('zone_permission');
		//dd($check_zone_permission);
		if($check_zone_permission != "Zone1"){
			return  redirect('/');
		}
		Session::save();
		return view('Zone7/index');
	}
	
	public function logout(Request $request)
	{
		
		$request->session()->flush();
	
		return  redirect('/');
	}
	
	
}

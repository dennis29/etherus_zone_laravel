<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class zone_permission_role extends Model
{
    protected $fillable = [
        'zone_permission_id','role_id'
    ];
}
